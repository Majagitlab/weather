import { Routes, RouterModule } from '@angular/router';

const APP_ROUTER: Routes = [
  {
    path: '**',
    //component: ErrorComponent,
    data: { title: '404 Not Found', message: 'You may be lost. Follow the breadcrumbs back <a href="/">home</a>.' } }
];

export const appRouting = RouterModule.forRoot(APP_ROUTER);
