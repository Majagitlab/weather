import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'app-hourly',
  templateUrl: './hourly.component.html',
  styleUrls: ['./hourly.component.scss']
})

export class HourlyComponent implements OnInit {
  loc$: Observable<string>;
  loc!: string;
  currentWeather: any = <any>{};
  hourly: any;
  hour: any[] = [];
  msg!: string;
  constructor(
    private store: Store<any>,
    private weatherService: WeatherService
  ) {
    this.loc$ = store.pipe(select('loc'));
  }

  ngOnInit() {
    this.loc$.subscribe(loc => {
      this.loc = loc;
      this.searchWeather(loc);
    })
  }
  searchWeather(loc: string) {
    this.msg = '';
    this.currentWeather = {};
    this.weatherService.getCurrentWeather(loc)
      .subscribe(res => {
        this.currentWeather = res;
        this.weatherService.getCurrent(this.currentWeather.coord.lat, this.currentWeather.coord.lon)
        .subscribe(res => {
          this.hourly = res;
          this.hour = this.hourly.hourly;
        }, err => {
  })
      }, err => {
}, () => {
       // this.searchUv(loc);
      })
  }
  searchUv(loc: string) {
    this.weatherService.getUv(this.currentWeather.coord.lat, this.currentWeather.coord.lon)
      .subscribe(res => {
        this.hourly = res;
        this.hour = this.hourly.hourly;
      }, err => {
})
  }
  resultFound() {
    return Object.keys(this.currentWeather).length > 0;
  }
}
