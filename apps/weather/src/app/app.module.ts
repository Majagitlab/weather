import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HomeComponent } from './features/home/home.component';
import { StoreModule } from '@ngrx/store';
import { locationReducer } from './reducers/location-reducer';
import { AppComponent } from './app.component';
import { WeatherDisplayComponent } from './features/weather/wether-display.component';
import { UvComponent } from './features/uv/uv.component';
import { ForecastComponent } from './features/forecast/forecast.component';
import { SearchBarComponent } from './features/search-bar/search-bar.component';
import { DailyComponent } from './features/daily/daily.component';
import { HourlyComponent } from './features/hourly/hourly.component';
import { MatTableModule } from '@angular/material/table';
import { FormsModule } from '@angular/forms';
import { WeatherService } from './services/weather.service';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [AppComponent,
    WeatherDisplayComponent,
    UvComponent,
    ForecastComponent,
    SearchBarComponent,
    HomeComponent,
    DailyComponent,
    HourlyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({
      loc: locationReducer
    }),
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatToolbarModule,
    MatInputModule,
    MatTabsModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
  ],
  providers: [
    WeatherService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class AppModule { }
