import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private _apiKey = '25821052162a6b5bc895479b413d183f';
  
  constructor(private http: HttpClient) { 
    
  }
  getCurrentWeather(loc: string) {
    return this.http.get(`${environment.apiUrl}/weather?q=loc&appid=${this._apiKey}`)
  }
  getCurrent(la: number, lo: number) {
    return this.http.get(`${environment.apiUrl}/onecall?lat=${la}&lon=${lo}&appid=${this._apiKey}`)
  }
  getForecast(loc: string) {
    return this.http.get(`${environment.apiUrl}/forecast?q=loc&appid=${this._apiKey}`)
  }
  getUv(la: number, lo: number) {
    let startDate = Math.round(+moment(new Date()).subtract(1, 'week').toDate() / 1000);
    let endDate = Math.round(+moment(new Date()).add(1, 'week').toDate() / 1000);
    return this.http.get(`${environment.apiUrl}/uvi/history?lat=${la}&lon=${lo}&start=${startDate}&end=${endDate}&appid=${this._apiKey}`)
  }
}