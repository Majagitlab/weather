import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherDisplayComponent } from './weather/wether-display.component';

@NgModule({
	declarations: [WeatherDisplayComponent],
	imports: [CommonModule],
})


export class WeatherForecastServicesModule {}
